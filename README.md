# Advent 101

An ultra-simplified version of the classic text adventure game Colossal Cave Adventure.
Written in XC=BASIC 3.1.2

# CREDITS
* Original development by Willie Crowther, 1976
* Greatly expanded by Don Woods, 1977
* Streamlined with random maps and new puzzles by Jeffrey Henning, 2017
* Ported to C64 XC=BASIC 3, bug-fixed and further touches by JJFlash, 2023
* XC=BASIC 3 by Csaba Fekete, 2022-2023