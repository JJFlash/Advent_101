' "AXE", "%", "A LITTLE DWARF #AXE IS HERE."
DATA AS WORD $0ADF
DATA AS BYTE $25
DATA AS WORD $0AE2
' "BIRDCAGE", "-", "A CHEERFUL LITTLE #BIRD IS SITTING IN A #CAGE SINGING."
DATA AS WORD $0AF8
DATA AS BYTE $2D
DATA AS WORD $0B00
' "CHEST", "-", "A LARGE TREASURE #CHEST IS HERE."
DATA AS WORD $0B2C
DATA AS BYTE $2D
DATA AS WORD $0B31
' "DIAMONDS", "$", "THERE ARE $DIAMONDS HERE!"
DATA AS WORD $0B4B
DATA AS BYTE $24
DATA AS WORD $0B53
' "EGGS", "$", "THERE IS A LARGE NEST HERE, FULL OF GOLDEN $EGGS!"
DATA AS WORD $0B69
DATA AS BYTE $24
DATA AS WORD $0B6D
' "FOOD", "-", "THERE IS A PACKAGE OF #FOOD HERE."
DATA AS WORD $0B95
DATA AS BYTE $2D
DATA AS WORD $0B99
' "GOBLET", "$", "THERE IS A JEWELED $GOBLET HERE."
DATA AS WORD $0BB3
DATA AS BYTE $24
DATA AS WORD $0BB9
' "HONEY", "-", "THERE IS A JAR OF #HONEY HERE."
DATA AS WORD $0BD3
DATA AS BYTE $2D
DATA AS WORD $0BD8
' "ITEM", "$", "A NONDESCRIPT METAL $ITEM GLINTS IN THE LAMPLIGHT."
DATA AS WORD $0BEF
DATA AS BYTE $24
DATA AS WORD $0BF3
' "JEWELRY", "$", "THERE IS PRECIOUS $JEWELRY HERE."
DATA AS WORD $0C1D
DATA AS BYTE $24
DATA AS WORD $0C24
' "KEYS", "%", "THERE IS A SET OF #KEYS HERE."
DATA AS WORD $0C3F
DATA AS BYTE $25
DATA AS WORD $0C43
' "LIVE MAP", "!", "A MAGICAL #LIVE #MAP OF THE CAVE IS HERE!"
DATA AS WORD $0C59
DATA AS BYTE $21
DATA AS WORD $0C60
' "MIRROR", "%", "THERE IS AN ANTIQUE HAND #MIRROR: GOOD WORKMANSHIP, BUT IT'S NOT VALUABLE."
DATA AS WORD $0C81
DATA AS BYTE $25
DATA AS WORD $0C87
' "NUGGET", "$", "THERE IS A LARGE SPARKLING $NUGGET OF GOLD HERE."
DATA AS WORD $0CC5
DATA AS BYTE $24
DATA AS WORD $0CCB
' "OIL", "-", "THERE IS A SMALL VIAL OF #OIL HERE."
DATA AS WORD $0CF2
DATA AS BYTE $2D
DATA AS WORD $0CF5
' "PERSIAN RUG", "$", "THERE IS A $PERSIAN $RUG SPREAD OUT ON THE FLOOR!"
DATA AS WORD $0D10
DATA AS BYTE $24
DATA AS WORD $0D1A
' "QUARTZ", "$", "A $QUARTZ CRYSTAL AMONGST THE RUBBLE HERE REFRACTS THE LIGHT OF YOUR LAMP."
DATA AS WORD $0D42
DATA AS BYTE $24
DATA AS WORD $0D48
' "ROD", "%", "A THREE FOOT BLACK #ROD WITH A RUSTY STAR ON AN END LIES NEARBY."
DATA AS WORD $0D85
DATA AS BYTE $25
DATA AS WORD $0D88
' "SPELLBOOK", "-", "THERE IS A #SPELLBOOK HERE, WITH MYSTERIOUS RUNES ON ITS COVER."
DATA AS WORD $0DBA
DATA AS BYTE $2D
DATA AS WORD $0DC3
' "TRIDENT", "$", "THERE IS A JEWEL-ENCRUSTED $TRIDENT HERE."
DATA AS WORD $0DF7
DATA AS BYTE $24
DATA AS WORD $0DFE
' "UMBRELLA", "+", "AN #UMBRELLA HAS BEEN LEFT OPEN TO DRY HERE."
DATA AS WORD $0E21
DATA AS BYTE $2B
DATA AS WORD $0E29
' "VASE", "$", "THERE IS A DELICATE, PRECIOUS >MING $VASE HERE!"
DATA AS WORD $0E4C
DATA AS BYTE $24
DATA AS WORD $0E50
' "WATER BOTTLE", "-", "THERE IS A FULL #WATER #BOTTLE HERE."
DATA AS WORD $0E78
DATA AS BYTE $2D
DATA AS WORD $0E83
' "", "", ""
DATA AS WORD $0000
DATA AS BYTE $00
DATA AS WORD $0000
' "", "", ""
DATA AS WORD $0000
DATA AS BYTE $00
DATA AS WORD $0000
' "ZINE", "-", "HERE IS A RECENT ISSUE OF THE '#ZINE `SPELUNKER >TODAY`."
DATA AS WORD $0EA0
DATA AS BYTE $2D
DATA AS WORD $0EA4
